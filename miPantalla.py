# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\miPantalla.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1378, 451)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 100, 141, 121))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.pushButton_create = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_create.setObjectName("pushButton_create")
        self.verticalLayout.addWidget(self.pushButton_create)
        self.pushButton_Ver = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_Ver.setObjectName("pushButton_Ver")
        self.verticalLayout.addWidget(self.pushButton_Ver)
        self.pushButton_Anadir = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_Anadir.setObjectName("pushButton_Anadir")
        self.verticalLayout.addWidget(self.pushButton_Anadir)
        self.pushButton_Eliminar = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_Eliminar.setObjectName("pushButton_Eliminar")
        self.verticalLayout.addWidget(self.pushButton_Eliminar)
        self.tableView = QtWidgets.QTableView(self.centralwidget)
        self.tableView.setGeometry(QtCore.QRect(160, 10, 1211, 391))
        self.tableView.setObjectName("tableView")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1378, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton_create.setText(_translate("MainWindow", "Crear Base de Datos"))
        self.pushButton_Ver.setText(_translate("MainWindow", "Ver Tabla de Registros"))
        self.pushButton_Anadir.setText(_translate("MainWindow", "Añadir Registro"))
        self.pushButton_Eliminar.setText(_translate("MainWindow", "Eliminar Registro"))
