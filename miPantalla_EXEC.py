import sys
from time import sleep

from PyQt5 import QtCore,QtWidgets
from miPantalla import Ui_MainWindow
from PyQt5 import QtSql
import sqlite3
from pprint import pprint

class MainWindow_EXEC():

    def __init__(self):
        app=QtWidgets.QApplication(sys.argv)

        self.MainWindow=QtWidgets.QMainWindow()
        self.ui=Ui_MainWindow()
        self.ui.setupUi(self.MainWindow)

        #self.create_DB()
        self.ui.pushButton_create.clicked.connect(self.create_DB)
        self.ui.pushButton_Ver.clicked.connect(self.print_data)
        self.model=None
        self.ui.pushButton_Ver.clicked.connect(self.sql_tableview_model)
        self.ui.pushButton_Anadir.clicked.connect(self.sql_add_row)
        self.ui.pushButton_Eliminar.clicked.connect(self.sql_delete_row)

        #self.init_tabs()

        self.MainWindow.show()
        sys.exit(app.exec_())
        

    def sql_delete_row(self):
        try:
            if self.model:
                self.model.removeRow(self.ui.tableView.currentIndex().row())
                print('Datos eliminados con éxito')
            else:
                self.sql_tableview_model()
        except Exception as e:
            print('Falla en sql_delete_row',e)

    def sql_add_row(self):
        try:
            if self.model:
                self.model.insertRows(self.model.rowCount(),1)
                print('Datos añadidos con éxito')
            else:
                self.sql_tableview_model()
        except Exception as e:
            print('Falla en sql_add_row',e)

    def sql_tableview_model(self):
        try:
            db=QtSql.QSqlDatabase.addDatabase('QSQLITE')
            db.setDatabaseName('inscripcion3.db')

            self.tableView=self.ui.tableView
            self.model=QtSql.QSqlTableModel()
            self.tableView.setModel(self.model)

            self.model.setTable('ficha2')
            self.model.setEditStrategy(QtSql.QSqlTableModel.OnFieldChange)
            self.model.select()
            self.model.setHeaderData(0,QtCore.Qt.Horizontal,'ine')
            self.model.setHeaderData(1,QtCore.Qt.Horizontal,'Nombre')
            self.model.setHeaderData(2,QtCore.Qt.Horizontal,'Apellido')
            self.model.setHeaderData(3,QtCore.Qt.Horizontal,'Dirección')
            self.model.setHeaderData(4,QtCore.Qt.Horizontal,'Fecha de nacimiento')
            self.model.setHeaderData(5,QtCore.Qt.Horizontal,'Sexo (H o M)')
            self.model.setHeaderData(6,QtCore.Qt.Horizontal,'Padre')
            self.model.setHeaderData(7,QtCore.Qt.Horizontal,'Madre')
            self.model.setHeaderData(8,QtCore.Qt.Horizontal,'Encargado')
            self.model.setHeaderData(9,QtCore.Qt.Horizontal,'Código del curso')
            self.model.setHeaderData(10,QtCore.Qt.Horizontal,'Fecha de inscripción')
        except Exception as e:
            print('Falla en sql_tableview',e)

    def print_data(self):
        try:
            sqlite_file='inscripcion3.db'
            conn=sqlite3.connect(sqlite_file)
            cursor=conn.cursor()

            cursor.execute("SELECT * FROM 'ficha2' ORDER BY ine")
            all_rows=cursor.fetchall()
            pprint(all_rows)

            conn.commit()
            conn.close()
        except Exception as e:
            print('Falla en print_data',e)

    def create_DB(self):
        try:
            db=QtSql.QSqlDatabase.addDatabase('QSQLITE')
            db.setDatabaseName('inscripcion3.db')
            db.open()

            query=QtSql.QSqlQuery()

            query.exec_("create table ficha2(ine int primary key, nombre varchar(20),"
                        "apellido varchar(20), direccion varchar(120), f_nac varchar(10),"
                        "sexo varchar(1), padre varchar(100), madre varchar(100),"
                        "encargado varchar(100), c_curso int, f_inscripcion varchar(10))")
            print('Base de datos creada.')
            
            query.exec_("insert into ficha2 values (1217100877, 'Jesús', 'García', 'Allende 34', '22/03/1999', 'H', 'Jesús García', 'Dora Alicia Corona', 'Anastacio', 12345, '21/10/2019')")
            print('Datos añadidos con éxito')
        except Exception as e:
            print('Falla en create_DB',e)
                
MainWindow_EXEC()
